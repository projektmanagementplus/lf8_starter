package de.szut.lf8_project.controller;

import de.szut.lf8_project.model.Employee;
import de.szut.lf8_project.service.EmployeeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RequestMapping("/api/employees")
@RestController
public class EmployeeController {

    final
    EmployeeService service;

    @Operation(summary = "Add an employee")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successfully created employee",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Employee.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid JSON supplied",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Resource not found",
                    content = @Content) })
    @PostMapping("")
    public ResponseEntity<Employee> create(@RequestBody Employee employee, @RequestHeader("Authorization") String accessToken) {
        return this.service.add(employee, accessToken);
    }

    @Operation(summary = "Gets a list of all employees")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of employees found",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Employee.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid request",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Employees not found",
                    content = @Content) })
    @GetMapping("")
    public ResponseEntity<Employee[]> getAll(@RequestHeader("Authorization") String accessToken) {
        return this.service.getAll(accessToken);
    }

    @Operation(summary = "Get an employee by their id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the employee",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Employee.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Employee not found",
                    content = @Content) })
    @GetMapping("/{id}")
    public ResponseEntity<Employee> getById(@PathVariable int id, @RequestHeader("Authorization") String accessToken) {
        return this.service.getById(id, accessToken);
    }

    @Operation(summary = "Update an employee by their id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully updated employee",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Employee.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid data supplied",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Resource not found",
                    content = @Content) })
    @PutMapping("/{id}")
    public ResponseEntity<Employee> update(@PathVariable int id, @RequestBody Employee employee, @RequestHeader("Authorization") String accessToken) {
        return this.service.update(id, employee, accessToken);
    }

    @Operation(summary = "Delete an employee by their id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Successfully deleted employee",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Employee.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Resource not found",
                    content = @Content) })
    @DeleteMapping("/{id}")
    public ResponseEntity<Employee> delete(@PathVariable int id, @RequestHeader("Authorization") String accessToken) {
        return this.service.delete(id, accessToken);
    }

}
