package de.szut.lf8_project.service;

import de.szut.lf8_project.HeaderFactory;
import de.szut.lf8_project.model.Employee;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@RequiredArgsConstructor
@Service
public class EmployeeService {

    final RestTemplate restTemplate;

    @Value("${employee-service.url}")
    private String employeeEndpoint;

    public ResponseEntity<Employee> add(Employee employee, String accessToken) {
        HttpHeaders header = HeaderFactory.createHeader(accessToken);
        HttpEntity<Employee> request = new HttpEntity<>(employee, header);

        try {
            return restTemplate.exchange(employeeEndpoint, HttpMethod.POST, request, Employee.class);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Employee[]> getAll(String accessToken) {
        HttpHeaders header = HeaderFactory.createHeader(accessToken);
        HttpEntity<Employee> request = new HttpEntity<>(header);

        try {
            return restTemplate.exchange(employeeEndpoint, HttpMethod.GET, request, Employee[].class);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Employee> getById(int id, String accessToken) {
        HttpHeaders header = HeaderFactory.createHeader(accessToken);
        HttpEntity<Employee> request = new HttpEntity<>(header);

        try {
            return this.restTemplate.exchange(employeeEndpoint + "/" + id,
                    HttpMethod.GET, request, Employee.class);
        } catch (HttpClientErrorException.NotFound e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Employee> update(int id, Employee employee, String accessToken) {
        HttpHeaders header = HeaderFactory.createHeader(accessToken);
        HttpEntity<Employee> request = new HttpEntity<>(employee, header);

        try {
            return this.restTemplate.exchange(employeeEndpoint + "/" + id, HttpMethod.PUT,
                    request, Employee.class);
        } catch (HttpClientErrorException.NotFound e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Employee> delete(int id, String accessToken) {
        HttpHeaders header = HeaderFactory.createHeader(accessToken);
        HttpEntity<Employee> request = new HttpEntity<>(header);

        try {
            return this.restTemplate.exchange(employeeEndpoint + "/" + id, HttpMethod.DELETE,
                    request, Employee.class);
        } catch (HttpClientErrorException.NotFound e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
