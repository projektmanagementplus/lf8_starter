package de.szut.lf8_project.service;

import de.szut.lf8_project.HeaderFactory;
import de.szut.lf8_project.model.Project;
import de.szut.lf8_project.repositories.ProjectRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ProjectService {

    final ProjectRepository repository;

    public Project add(Project project, String accessToken) throws Exception {
        HttpHeaders header = HeaderFactory.createHeader(accessToken);
        try {
            return repository.save(project);
        } catch (Exception e) {
            throw new Exception();
        }
    }

    public List<Project> getAll(String accessToken) throws Exception {
        HttpHeaders header = HeaderFactory.createHeader(accessToken);
        try {
            return repository.findAll();
        } catch (Exception e) {
            throw new Exception();
        }
    }

    public Project get(int id, String accessToken) throws Exception {
        HttpHeaders header = HeaderFactory.createHeader(accessToken);
        try {
            Optional<Project> optional = repository.findById(id);
            return optional.orElse(null);
        } catch (Exception e) {
            throw new Exception();
        }
    }

    public Project update(int id, Project project, String accessToken) throws Exception {
        HttpHeaders header = HeaderFactory.createHeader(accessToken);
        try {
            Optional<Project> optional = repository.findById(id);
            if (optional.isPresent()) {
                Project persistentProject = optional.get();
                persistentProject.setTitle(project.getTitle());
                persistentProject.setHeadID(project.getHeadID());
                persistentProject.setClientID(project.getClientID());
                persistentProject.setContactName(project.getContactName());
                persistentProject.setDescription(project.getDescription());
                persistentProject.setDateOfStart(project.getDateOfStart());
                persistentProject.setDateOfPlannedEnd(project.getDateOfPlannedEnd());
                persistentProject.setDateOfActualEnd(project.getDateOfActualEnd());
                persistentProject = repository.save(persistentProject);
                return persistentProject;
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new Exception();
        }
    }

    public void delete(int id, String accessToken) throws Exception {
        HttpHeaders header = HeaderFactory.createHeader(accessToken);
        try {
            repository.deleteById(id);
        } catch (Exception e) {
            throw new Exception();
        }
    }

    public void delete(Project project, String accessToken) throws Exception {
        HttpHeaders header = HeaderFactory.createHeader(accessToken);
        try {
            repository.delete(project);
        } catch (Exception e) {
            throw new Exception();
        }
    }
}
