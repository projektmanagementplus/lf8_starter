package de.szut.lf8_project;

import org.springframework.http.HttpHeaders;

public class HeaderFactory {

    public static HttpHeaders createHeader(String accessToken) {
        HttpHeaders header = new HttpHeaders();
        header.setBearerAuth(extractAccessToken(accessToken));
        return header;
    }

    public static String extractAccessToken(String token) {
        return token.substring(token.indexOf(" "));
    }
}
