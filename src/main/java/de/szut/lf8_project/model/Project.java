package de.szut.lf8_project.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@RequiredArgsConstructor
@Entity
@Table(name = "tab_project")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "projectID")
    private int projectID;

    @Column(name = "title")
    private String title;

    @Column(name = "headID")
    private int headID;

    @Column(name = "clientID")
    private int clientID;

    @Column(name = "contact_name")
    private String contactName;

    @Column(name = "description")
    private String description;

    @Column(name = "date_of_start")
    private Date dateOfStart;

    @Column(name = "date_of_planned_end")
    private Date dateOfPlannedEnd;

    @Column(name = "date_of_actual_end")
    private Date dateOfActualEnd;
}
