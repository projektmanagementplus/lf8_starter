package de.szut.lf8_project.repositories;

import de.szut.lf8_project.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, Integer> {
}
